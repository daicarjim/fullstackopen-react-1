import React from 'react';
import ReactDOM from 'react-dom';


const Hello = (props) => {

  return (
    <div>
      <p>Hola {props.name}, tu tienes {props.age} años de edad</p>
    </div>

  )

}


const App = () => {

const now = new Date().toString()
const a = 10
const b = 20

  return (
   <>
     <p>Hello World, <br /> It is {now}</p>
     <p>
       {a} + {b} = {a + b}
     </p>
     <Hello name={"Mundo"} age={a + b}/>
     <Hello name={"jajaja"} age={14 + 80}/>
     <Hello name={now} age={"NINGUN"}/>
     
  </>
 
  )}



ReactDOM.render(<App />, document.getElementById("root"))

